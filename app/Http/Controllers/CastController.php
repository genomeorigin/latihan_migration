<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function buat(){
        return view('/project1/cast/create');
    }

    public function tambah(Request $request){
        $request->validate([
            'nama' => 'required',      
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')->insert(
        [
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);

        return redirect('/cast');
    }

    public function indexx(){
        $cast = DB::table('cast')->get();

        return view('/project1/cast/index', compact('cast'));
    }

    public function tampil($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('/project1/cast/tampil', compact('cast'));
    }

    public function ubah($id){
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('/project1/cast/ubah', compact('cast'));
    }

    public function perbarui($id, Request $request){
        $request->validate([
            'nama' => 'required',      
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $affected = DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );
        
        return redirect('/cast');
    }

    public function hapus($id){
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
