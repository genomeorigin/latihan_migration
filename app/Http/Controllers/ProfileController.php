<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('user_id', Auth::id());

        return view('project1.profile.index', compact('profile'));
    }
}
