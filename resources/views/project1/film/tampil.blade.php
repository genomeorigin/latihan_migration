@extends('layout.master')

@section('judul')
Detail Film id ke {{$film->id}}
@endsection

@section('judul2')
Film Box Catering
@endsection

@section('content')

<img src="{{asset('images/'.$film->poster)}}" height="650px" alt="">
<h4>{{$film->judul}}</h4>
<p>{{$film->ringkasan}}</p>

<a href="/film" class="btn btn-secondary">Kembali</a>

@endsection