@extends('layout.master')

@section('judul')
Create Data Game
@endsection

@section('judul2')
Data Game
@endsection

@section('content')

<form action="/game" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gameplay</label>
        <input type="text" class="form-control" name="gameplay" placeholder="Masukkan Usia">
        @error('gameplay')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Developer</label>
        <input type="text" class="form-control" name="developer" placeholder="Masukkan Dev">
        @error('developer')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Year</label>
        <input type="number" class="form-control" name="year" placeholder="Masukkan Year">
        @error('year')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection